# Environment setup project

## Bash version

### Requirements

Curl, git and vim must be installed on the machine

### Usage

```bash
git clone https://gitlab.com/fcontesse/env-setup
cd env-setup
./bash-setup.sh
```

## Ansible version

### Requirements

Only Ansible needs to be installed. Works with all distros.

### Usage

Packages to install can be configured in ./vars./packages.yml

Vim plugins to install can be configured in ./vars/vim.yml

```bash
git clone https://gitlab.com/fcontesse/env-setup
cd env-setup
./ansible-setup.sh
```
