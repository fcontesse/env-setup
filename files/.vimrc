"""""""" Sections
"
"  1.General
"  2.UI
"  3.Search
"  4.Tabs&Indents
"  5.Misc
"  6.Plugins
"
"""""""" 1.General

  execute pathogen#infect()

  set history=500

  " Set to auto read when a file is changed from the outside
  set autoread

  " Set the leader key to ','
  let mapleader = ","

  " :W sudo saves the file
  command W w !sudo tee % > /dev/null

  " When opening a file, keep the current one open instead of closing it
  set hidden

  " Set utf8 as standard encoding and en_US as the standard language
  set encoding=utf8

  " Use Unix as the standard file type
  set ffs=unix,dos,mac

"""""""" 2.UI

  colorscheme desert
  set background=dark

  " Toggle relative numbering when entering insert mode...
  "augroup numbertoggle
  "  autocmd!
  "  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
  "  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
  "augroup END

  "...with absolute numbering enabled allow switch between hybrid and absolute
  set relativenumber
  set number

  " Change line numbering column color
  highlight LineNr ctermfg=grey ctermbg=black

  " Highlight colomn 80
  if exists('+colorcolumn')
    set colorcolumn=80
    highlight ColorColumn ctermbg=black
  else
    au BufWinEnter * let w:m2=matchadd('ErrorMsg', '\%>80v.\+', -1)
  endif

  " Highlight current line
  highlight CursorLine   cterm=NONE ctermbg=black
  set cursorline

  " Set 7 lines to the cursor - when moving vertically using j/k
  set so=7

  " Height of the command bar
  set cmdheight=2

  "Always show current position
  set ruler

  " Don't redraw while executing macros (good performance config)
  set lazyredraw

  " Show matching brackets when text indicator is over them
  set showmatch
  " How many tenths of a second to blink when matching brackets
  set mat=2

"""""""" 3.Search

  " Ignore case when searching
  set ignorecase

  " When upper-case is typed in search, search is case-sensitive
  set smartcase


  " Highlight search results
  set hlsearch

  " Move the cursor to the matched string while typing the search pattern
  set incsearch

  " Extended regular expression (as opposed to grep-like comportment)
  set nomagic

  " Visual mode pressing * or # searches for the current selection
  vnoremap <silent> * :<C-u>call VisualSelection('', '')<CR>/<C-R>=@/<CR><CR>
  vnoremap <silent> # :<C-u>call VisualSelection('', '')<CR>?<C-R>=@/<CR><CR>

  " Disable highlight when <leader><cr> is pressed
  map <silent> <leader><cr> :noh<cr>

"""""""" 4.Tabs&Indents

  " Use spaces instead of tabs
  set expandtab

  " 1 tab == 2 spaces
  set shiftwidth=2
  set tabstop=2

  " Linebreak on 80 characters
  "set lbr
  "set tw=80

  " Auto indent
  set ai
  " Smart indent
  set si

  " Wrap lines and use ">  \" instead of ">   " to prepend wrapped lines
  set wrap

"""""""" 5. Misc

  " Deletes trailing whitespaces on leader + space
  fun! CleanExtraSpaces()
    let save_cursor = getpos(".")
    let old_query = getreg('/')
    silent! %s/\s\+$//e
    call setpos('.', save_cursor)
    call setreg('/', old_query)
  endfun
  nnoremap <silent> <Leader><space> :<C-u>call CleanExtraSpaces()<CR>

  " Remove the Windows ^M on leader + m 
  noremap <Leader>m mmHmt:%s/<C-V><cr>//ge<cr>'tzt'm


""""""" 6. Plugins

  " Lint
  let g:syntastic_always_populate_loc_list = 1
  let g:syntastic_auto_loc_list = 1
  let g:syntastic_check_on_open = 1
  let g:syntastic_check_on_wq = 0

  let g:syntastic_yaml_checkers = ['yamllint']
  let g:syntastic_yamllint_config_file = ['~/.yamllint']
  let g:syntastic_ansible_checkers = ['ansible_lint']

  au BufNewFile,BufRead *.yaml set filetype=yaml.ansible
  au BufNewFile,BufRead *.yml set filetype=yaml.ansible

  let g:syntastic_markdown_mdl_exec = 'markdownlint'
  let g:syntastic_markdown_mdl_args = ''

  let g:syntastic_checkers_dockerfile = 'dockerfile_lint'

  " Indent
  let g:indent_guides_enable_on_vim_startup = 1
  let g:indent_guides_auto_colors = 0
  " autocmd VimEnter,Colorscheme * :hi IndentGuidesOdd  guibg=red   ctermbg=grey
  autocmd VimEnter,Colorscheme * :hi IndentGuidesEven guibg=green ctermbg=black

