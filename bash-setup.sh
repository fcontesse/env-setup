#!/bin/bash

green='\e[32m'
red='\e[31m'
default='\e[39m'

githubraw='https://raw.githubusercontent.com'
pathogen='/tpope/vim-pathogen/master/autoload/pathogen.vim'
err_file='./setup.err'

vimplugins=(
  https://github.com/tpope/vim-fugitive
  https://github.com/tpope/vim-surround
  https://github.com/tpope/vim-repeat
  https://github.com/tpope/vim-commentary
  https://github.com/scrooloose/nerdtree
  https://github.com/scrooloose/syntastic
  https://github.com/kien/ctrlp.vim
  https://github.com/vim-scripts/ReplaceWithRegister
  https://github.com/michaeljsmith/vim-indent-object
  https://github.com/kana/vim-textobj-user
  https://github.com/kana/vim-textobj-entire
  https://github.com/kana/vim-textobj-line
  https://github.com/vim-airline/vim-airline
  https://github.com/vim-airline/vim-airline-themes
)

echo -en 'copying .bashrc\t\t\t'
cp ./files/.bashrc ~/
if [ $? -ne 0 ]
then
  echo -e "$red""failed to download .bashrc\t exiting""$default"
  exit 0
fi
echo -e "$green""done""$default"

echo -en 'copying .vimrc\t\t\t'
cp ./files/.vimrc ~/
if [ $? -ne 0 ]
then
  echo -e "$red""failed to download .vimrc\t exiting""$default"
  exit 0
fi
echo -e "$green""done""$default"


cd ~

if [ ! -d .vim/autoload ]
then
  echo -en 'creating .vim/autoload\t\t'
  mkdir -p .vim/autoload
  if [ ! -d .vim/autoload ]
  then
    echo "$red""error creating .vim/autoload\t exiting""$default"
    exit 1
  fi
  echo -e "$green""done""$default"
fi

if [ ! -d .vim/bundle ]
then
  echo -en 'creating .vim/bundle\t\t'
  mkdir -p .vim/bundle
  if [ ! -d .vim/bundle ]
  then
    echo -e "$red""error creating .vim/bundle\t exiting""$default"
    exit 1
  fi
  echo -e "$green""done""$default"
fi

echo -en 'downloading pathogen\t\t'
curl $githubraw$pathogen -o .vim/autoload/pathogen.vim &>/dev/null
if [ ! -s .vim/autoload/pathogen.vim ]
then
  echo -e "$red""failed to download pathogen\t exiting""$default"
  exit 1
fi
echo -e "$green""done""$default"

echo -e "Installing/updating plugins :"
for plugin in ${vimplugins[@]};
do
  cd ~/.vim/bundle
  printf '%s%-55s' '- ' $plugin
  git clone -q $plugin 2>/dev/null
  if [ $? -ne 0 ]
  then
    cd $(basename $plugin)
    git pull -q
    if [ $? -ne 0 ]
    then
      echo -e "$red""failed""$default"
    else
      echo -e "$green""updated""$default"
    fi
  else
    echo -e "$green""installed""$default"
  fi
done
